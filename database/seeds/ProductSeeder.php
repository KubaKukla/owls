<?php

use App\User;
use App\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'admin@email.com')->firstOrFail();

        $product = new Product();
        $product->name = 'Test product';
        $product->price = 100;
        $product->user()->associate($user);
        $product->save();
    }
}
