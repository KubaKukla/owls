<b>DISCOUNT</b><br>
Product: {{ $productName }}<br>
Original price: {{ $originalPrice }}<br>
Discount price: {{ $discountPrice }}<br>
<a href="{{route('product.show', $productId)}}">Click!</a>
