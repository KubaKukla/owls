@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <div class="card">
                <div class="card-header">Add discount</div>

                <div class="card-body">
                    <form method="POST" action="/discount/store">
                        @csrf
                        <select name="product" id="product">
                            @foreach ($products as $product)
                                <option value="{{ $product->id }}">{{ $product->name }}</option>
                            @endforeach
                        </select>
                        <input name="value" type="number" placeholder="Value" min="0" max="100">
                        <input name="activation_date" type="date">
                        <input type="submit" value="send">
                    </form>
                </div>
            </div>

            <div class="card mt-5">
                <div class="card-header">Discounts</div>

                <div class="card-body">
                    <table cellpadding="10">
                        <thead>
                            <tr>
                                <td>Product</td>
                                <td>Discount value</td>
                                <td>Activation date</td>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($discounts as $discount)
                            <tr>
                                <td>{{ $discount->product->name }}</td>
                                <td>{{ $discount->value }}</td>
                                <td>{{ $discount->activation_date }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

                <div class="card mt-5">
                    <div class="card-header">Products</div>

                    <div class="card-body">
                        <table cellpadding="10">
                            <thead>
                            <tr>
                                <td>Name</td>
                                <td>Price</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($products as $product)
                                <tr>
                                    <td>{{ $product->name }}</td>
                                    <td>{{ $product->price }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </div>
</div>
@endsection
