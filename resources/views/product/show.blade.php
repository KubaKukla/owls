@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="card">
                    <div class="card-header">Product</div>

                    <div class="card-body">
                        {{ $product->name }}<br>
                        Price: {{ $product->price }}<br><br>
                        Active promotions:<br>
                        @foreach($product->discount as $discount)
                            {{-- z polem active dla promocji było by ładniej :) --}}
                            @if($discount->activation_date === null || $discount->activation_date <= \Carbon\Carbon::today()->format('Y-m-d'))
                                Discount value: {{ $discount->value }}<br>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
