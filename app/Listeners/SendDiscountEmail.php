<?php

namespace App\Listeners;

use App\Events\DiscountStored;
use App\Jobs\SendDiscountEmail as SendDiscountEmailJob;

class SendDiscountEmail
{
    public function handle(DiscountStored $event)
    {
        if (!$event->discount->activation_date) {
            SendDiscountEmailJob::dispatch($event->discount);
        }
    }
}
