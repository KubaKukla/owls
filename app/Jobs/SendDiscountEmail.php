<?php

namespace App\Jobs;

use App\Discount;
use App\Email;
use App\Mail\SendDiscountMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendDiscountEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $discount;

    /**
     * Create a new job instance.
     *
     * @param Discount $discount
     */
    public function __construct(Discount $discount)
    {
        $this->discount = $discount;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $emails = Email::where('value', '<=', $this->discount->value)->get();
        Mail::to($emails->pluck('email'))->send(new SendDiscountMail($this->discount));
    }
}
