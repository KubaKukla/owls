<?php

namespace App\Jobs;

use App\Discount;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessDailyEmails implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function handle()
    {
        $date = Carbon::today()->format('Y-m-d');
        $discounts = Discount::where('activation_date', $date)->get();

        foreach ($discounts as $discount) {
            SendDiscountEmail::dispatch($discount);
        }
    }
}
