<?php

namespace App\Mail;

use App\Discount;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendDiscountMail extends Mailable
{
    use Queueable, SerializesModels;

    public $discount;

    /**
     * Create a new message instance.
     *
     * @param Discount $discount
     */
    public function __construct(Discount $discount)
    {
        $this->discount = $discount;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $productId = $this->discount->product->id;
        $productName = $this->discount->product->name;
        $originalPrice = $this->discount->product->price;
        $discountPrice = $this->discount->product->price - $this->discount->product->price
            * $this->discount->value / 100;

        return $this->view('emails.discount')->with([
            'productName' => $productName,
            'productId' => $productId,
            'originalPrice' => $originalPrice,
            'discountPrice' => $discountPrice,
        ])->subject($productName. ' DISCOUNT PRICE '. $discountPrice);
    }
}
