<?php

namespace App\Http\Controllers;

use App\Events\DiscountStored;
use App\Http\Requests\StoreDiscountRequest;
use App\Product;
use App\Discount;
use Illuminate\Support\Facades\Auth;

class DiscountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(StoreDiscountRequest $request)
    {
        $discount = new Discount();
        $discount->value = $request->value;
        $discount->activation_date = $request->activation_date;
        $discount->product()->associate(Product::findOrFail($request->product));
        $discount->user()->associate(Auth::user());
        $discount->save();

        event(new DiscountStored($discount));

        return redirect()->route('home')->with('status','discount stored successfully');
    }
}
