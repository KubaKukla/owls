<?php

namespace App\Http\Controllers;

use App\Email;
use App\Http\Requests\StoreEmailRequest;
use Illuminate\Http\Request;

class EmailController extends Controller
{
    public function store(StoreEmailRequest $request)
    {
        $email = new Email();
        $email->email = $request->email;
        $email->value = $request->value;
        $email->save();

        return redirect()->route('welcome')->with('status','email stored successfully');
    }
}
