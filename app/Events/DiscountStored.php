<?php

namespace App\Events;

use App\Discount;
use Illuminate\Queue\SerializesModels;

class DiscountStored
{
    use SerializesModels;

    public $discount;

    public function __construct(Discount $discount)
    {
        $this->discount = $discount;
    }
}
