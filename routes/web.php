<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::name('email.')->group(function () {
    Route::post('/email/store', 'EmailController@store')->name('store');
});

Route::name('discount.')->group(function () {
    Route::post('/discount/store', 'DiscountController@store')->name('store');
});

Route::name('product.')->group(function () {
    Route::get('/product/{id}', 'ProductController@show')->name('show');
});


Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');
